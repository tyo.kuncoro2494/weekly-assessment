import React from 'react';
import { StyleSheet, Text, TextInput, View, ImageBackground, TouchableOpacity } from 'react-native';
import Cour from './Cour';


const SignUp = () => {
  return (
    <ImageBackground style={{ flex: 1, justifyContent: "center", alignItems: "center" }} source={require('./img/bg.png')}>
      <Cour style={styles.txt}>Sign Up</Cour>
      <View>
        <TextInput style={styles.userName} placeholder="User Name" placeholderTextColor="grey" />
        <TextInput style={styles.email} placeholder="Email" placeholderTextColor="grey" />
        <TextInput style={styles.password} secureTextEntry={true} placeholder="Password" placeholderTextColor="grey" borderBottom />
        <TextInput style={styles.cpassword} secureTextEntry={true} placeholder="Confirm Password" placeholderTextColor="grey" />
        <TouchableOpacity>
          <View style={styles.button}>
            <Cour style={styles.buttonText}>Sign Up</Cour>
          </View>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  txt: {
    fontSize: 50
  },
  userName: {
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 1
  },
  email: {
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    marginTop: 5
  },
  password: {
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    marginTop: 5
  },
  cpassword:{
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    marginTop: 5
  },
  button: {
    marginTop: 30,
    width: 250,
    alignItems: 'center',
    backgroundColor: "white",
    borderWidth: 2
  },
  buttonText: {
    fontSize: 35,
    textAlign: 'center',
    paddingHorizontal: 20,
    color: 'grey'
  }
});

export default SignUp;