import React from 'react'
import { View, Text } from 'react-native'

export default function Cour(props) {
    return (

        <Text style= {[{fontFamily: "cour"},props.style]}>{props.children}</Text>
    )
}
