import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View, ImageBackground, TouchableOpacity } from 'react-native';
import Cour from './Cour';

const SignIn = () => {
  // const [coba, setCoba] = useState("")
  return (
    <ImageBackground style={{ flex: 1, justifyContent: "center", alignItems: "center" }} source={require('./img/bg.png')}>
      <Text style={styles.txt}>Sign In</Text>
      <View>
        <TextInput style={styles.userName} placeholder="User Name" placeholderTextColor="grey" />
        <TextInput style={styles.password} secureTextEntry={true} placeholder="Password" placeholderTextColor="grey" borderBottom />
        <TouchableOpacity>
          <View style={styles.button}>
            <Cour style={styles.buttonText}>Sign In</Cour>
          </View>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  txt: {
    fontSize: 50,
    fontFamily: "cour"
  },
  userName: {
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 2,
    marginBottom: 5
  },
  password:{
    width: 250,
    backgroundColor: 'white',
    borderBottomWidth: 2
  },
  button: {
    marginTop: 30,
    width: 250,
    alignItems: 'center',
    backgroundColor: "white",
    borderWidth: 2
  },
  buttonText: {
    fontSize: 35,
    textAlign: 'center',
    paddingHorizontal: 20,
    color: 'grey'
  }
});

export default SignIn;