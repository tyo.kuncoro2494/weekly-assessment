import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import Cour from './Cour';

const Home = () => {
  return (
    <ImageBackground style={{ flex: 1, justifyContent: "space-evenly" }} source={require('./img/bg.png')}>
      <Text></Text>
      <Text></Text>
      <View style={{ borderWidth: 2, borderColor: "white", marginHorizontal: 50 }}>
        <Cour style={styles.wtxt}>Wellcome</Cour>
        <Cour style={styles.wtxt}>Sign In or Sign Up</Cour>
      </View>
      <View style={styles.container}>
        <TouchableOpacity>
          <View style={styles.button}>
            <Cour style={styles.buttonText}>Sign In</Cour>
          </View>
        </TouchableOpacity><TouchableOpacity>
          <View style={styles.button}>
            <Cour style={styles.buttonText}>Sign Up</Cour>
          </View>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center"
  },
  button: {
    width: 200,
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 2
  },
  buttonText: {
    fontSize: 35,
    textAlign: 'center',
    color: 'grey'
  },

  wtxt: {
    textAlign: "center",
    fontSize: 50,
    color: "white",
  },
});

export default Home;
